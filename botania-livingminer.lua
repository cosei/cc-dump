--
-- Created with Jetbrains PyCharm.
-- User: boisei0
-- Date: 9/26/14
-- Time: 7:00 PM
--

n_blocks_length = 4
n_blocks_width = 4

function dig_row()
    local row_length = n_blocks_length * 3 - 1
    for i=1, row_length do
        turtle.digDown()
        turtle.forward()
    end
    turtle.digDown()
end

function dig_alternate_row()
    for i=1, n_blocks_length do
        turtle.digDown()
        turtle.forward()
        turtle.forward()
        turtle.digDown()
        turtle.forward()
    end
end

function dig_blocks_length_forward()
    dig_row()
    turtle.turnRight()
    turtle.forward()
    turtle.turnRight()
    dig_alternate_row()
    turtle.turnLeft()
    turtle.forward()
    turtle.turnLeft()
    dig_row()
end

function dig_blocks_length_back()
    dig_row()
    turtle.turnLeft()
    turtle.forward()
    turtle.turnLeft()
    dig_alternate_row()
    turtle.turnRight()
    turtle.forward()
    turtle.turnRight()
    dig_row()
end

function dig_grid()
    local width = n_blocks_width * 3
    local times = n_blocks_width / 2
    for i=1, times do
        dig_blocks_length_forward()
        turtle.turnRight()
        turtle.forward()
        turtle.turnRight()
        dig_blocks_length_back()
        turtle.turnLeft()
        turtle.forward()
        turtle.turnLeft()
    end
    turtle.turnLeft()
    for i=1, width do
       turtle.forward()
    end
    turtle.turnRight()
end

dig_grid()