transmuteReq = 4

-- Info about how to set it up can be seen here http://ftb.aacoba.net/files/turtle.gif
-- (c) Aacoba@aacoba.net Attribution-NonCommercial-ShareAlike 3.0 Unported
-- http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US

m = peripheral.wrap("right") -- Wrap the stone

function refillM()
	turtle.select(16) -- Select the bottom right slot
	if not m.rechargeMinium() then -- Check if recharging is possible if not, display error, and turn on the error redstone wire
		turnOnLight()
		print("Minium Stone not found!")
	end
	turtle.select(1) -- Return to the default item slot
end

function checkM() -- Check if the minium stone has less then  64 charge left, if false: try to refill
	while (m.getMiniumCharge() < 64) do 
		refillM() 
	end
	turnOffLight()
end


function transmute()
	while (turtle.getItemCount(1) >= transmuteReq) do -- Check if there are more then x items to transmute(x is required )
		m.transmuteItem(transmuteReq)
	end
	print("Minium Stone left : " .. m.getMiniumCharge()) -- Display the amount of charge left
end

function turnOnLight() -- Turn on the warning signal 
	rs.setOutput("left", true)
end

function turnOffLight() -- Turn off the warning Signal
	rs.setOutput("left", false)
end

function finishItem() -- Dumps the residual to the top and the product to the front
	while (turtle.getItemCount(2) > 0 ) do
		turtle.select(2)
		while not turtle.transferTo(1) do
			turtle.select(1)
			turtle.dropUp()
			turtle.select(2)
		end
		turtle.select(1)
		while not turtle.drop() do 
			print("Cant drop item!")
			sleep(1)

		end
	end	
end

function getNewItem() -- Fetches new item
	rs.setOutput("back", true)
	sleep(1)
	rs.setOutput("back", false)
	sleep(3)
end

function main() -- Main function
	while true do
		checkM()
		getNewItem()
		transmute()
		finishItem()

	end
end

main() -- Call the main function
