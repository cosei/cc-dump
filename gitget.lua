-- +---------------------+
-- |       LICENCE       |
-- +---------------------+

-- +----------------------------------------------------------------------------------+
-- | CC-Dump/FeedTheTrains Script to pull scripts from our repo's                     |
-- | Copyright (C) 2013  Auke Bakker Aacoba@Aacoba.net                                |
-- +----------------------------------------------------------------------------------+
-- | This program is free software; you can redistribute it and/or                    |
-- | modify it under the terms of the GNU General Public License                      |
-- | as published by the Free Software Foundation; either version 2                   |
-- | of the License, or (at your option) any later version.                           |
-- |                                                                                  |
-- | This program is distributed in the hope that it will be useful,                  |
-- | but WITHOUT ANY WARRANTY; without even the implied warranty of                   |
-- | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    |
-- | GNU General Public License for more details.                                     |
-- |                                                                                  |
-- | You should have received a copy of the GNU General Public License                |
-- | along with this program; if not, write to the Free Software                      |
-- | Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.  |
-- +----------------------------------------------------------------------------------+

-- +--------------------+
-- |       CONFIG       |
-- +--------------------+

args = {...}
hostUrl = "http://ftb.aacoba.net/gitget/"
repoURL = ""

-- +--------------------+
-- |        TODO        |
-- +--------------------+
-- | Add normal mode    |
-- | Add gitget list    |
-- |                    |
-- |                    |
-- +--------------------+

-- +--------------------+
-- |        CODE        |
-- +--------------------+

__author__ = "aacoba@aacoba.net"
__version__ = "v1"

function FatalError(msg)
	print("Fatal error:" .. msg)
	return false
end

function ParamaterParser()
	if #args < 2 then
		FatalError("Missing parameters")
		return false
	end
	if string.lower(args[1]) == "raw" then
		if #args < 4 then
			FatalError("Missing parameters for RAW functionality (RAW REPO REMOTENAME LOCALNAME)")
		else
			repoURL = repoUrlDef(args[2])
		end
	else
		FatalError("Not yet implemented, Please use RAW REPO REMOTENAME LOCALNAME")
	end

end

function repoUrlDef(repo)
	repo = string.lower(repo)
	if repo == "feedthetrains" then
		repoURL = "FeedTheTrains/"
	elseif repo == "ftt" then
		repoURL = "FeedTheTrains/"
	elseif repo == "cc-dump" then
		repoURL = "cc-dump/"
	elseif repo == "ccdump" then
		repoURL = "cc-dump/"
	elseif repo == "ccd" then
		repoURL = "cc-dump/"
	else
		return false
	end
	return repoURL
end

function download(url, of)
	local response = http.get(url)
	local f = fs.open(of, "w")
	f.write(response.readAll())
	f.close()
end


ParamaterParser()
print("Gitget: " .. __version__ .. " By " .. __author__)
print("Saving " .. hostUrl .. repoURL .. args[3] .. " as " .. args[4])

download(hostUrl .. repoURL .. args[3] .. ".lua" , args[4])