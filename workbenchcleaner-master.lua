-- Master Workbench Cleaner

--[[
	A program that detects if the pressure plate on top has 
	not been activated for the last 60 seconds, when 60s 
	have passed, it fires 20 pulses to the filter in front to empty the projecttable
--]]

__author__ = 'aacoba'
__version__ = '0.1'

function empty()
	x = 0
	print("Clearing Workbench")
	while x < 20 do
		rs.setOutput("back", true)
		sleep(0.1)
		rs.setOutput("back", false)
		sleep(0.1)
		x = x + 1
		print(x)
	end
	print("Workbench should been Cleared")
end

function main()
	tick = 0
	while true do
		if not rs.getInput("top") and not rs.getInput("left") then
			tick = tick + 1
			print("tick: " .. tick)
			sleep(1)
		else
			tick = 0
			print("Cleared ticks")
		end

		if (tick >= 60) then
			empty()
			tick = 0
		end
	end
end

main()