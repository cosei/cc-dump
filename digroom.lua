-- Run at your own risk :D
-- Got improvements? Leave a comment!

local args = {...}
local isAheadClear = false

-- Return the count of parameters.
function countParams()
   if #args ~= 3 then
    print('Missing arguments.')
    help()
    return 0
  end
end

function message(message)
  rednet.send(35, tostring(message))
end

function help()
  print('Run using l h w [sleep]')
end

-- Dig a row, with depth and return to start position when done.
function digRow(length, sleep_time)

  message('digRow')
  message(length)
  print(sleep_time)
  
  -- Dig
  for i=1, length do
    turtle.dig()
    sleep(sleep_time)

    -- Check if gravel fell down
    isAheadClear = false
    while isAheadClear == false do
      if turtle.detect() then
        message(tostring(os.clock()))
	      message('Detected block')
        turtle.dig()
        sleep(sleep_time)
      else
        -- print('Ahead is clear')
        isAheadClear = true
        turtle.forward()
      end
    end
  end
  
  -- Return to beginning
  turn180()
  for i=1, length do
    turtle.forward()
  end
  turn180()

end

-- Move the turtle to the right, while after moving facing the same position
function shiftLeft()
  turtle.turnLeft()
  turtle.dig()
  turtle.forward()
  turtle.turnRight()
end

-- Move the turtle to the right, while after moving facing the same position
function shiftRight()
  turtle.turnRight()
  turtle.dig()
  turtle.forward()
  turtle.turnLeft()
end

-- Rotate the turtle 180 degrees
function turn180()
  turtle.turnRight()
  turtle.turnRight()
end

function digRoom(length, width, height, sleep_time)

  for w=1,width do
    
    if w > 1 then
      shiftRight()
    end

    for h=1,height do
      digRow(length, sleep_time)
      if h < height then
	      turtle.digUp()
        turtle.up()
      end
    end

    -- Move the turtle back the bottom position
    for h=1, (height - 1) do
      turtle.down()
    end

  end

end

-- Program start.
rednet.open("right")
message("Turtle started.")
message(tostring(os.clock()))
message("Turtle connected")
message("Start fuel level:")
message(turtle.getFuelLevel())

args[1] = tonumber(args[1])
args[2] = tonumber(args[2])
args[3] = tonumber(args[3])

if args[4] == nil then
  print("it is nil")
  args[4] = 0.5
else 
  print("it is NOT nil")
  args[4] = tonumber(args[4])
end

print(args[1], type(args[1]))
print(args[2], type(args[2]))
print(args[3], type(args[3]))
print(args[4], type(args[4]))

digRoom(args[1], args[2], args[3], args[4])

-- Return to start position
turtle.turnLeft()
for i=2,args[2] do
  turtle.forward()
end
turtle.turnRight()

rednet.close("right")

message("Done")
message("Fuel left:");
message(tostring(turtle.getFuelLevel()))