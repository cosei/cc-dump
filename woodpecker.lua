local args = {...}

local height = 0

local slot_fuel = 16
local slot_saplings = 15
local slot_bonemeal = 14

function cutRow(treeCount, trunkSize)

  for i=1,treeCount do
    while true do
    
      if turtle.detect() then
        -- tree detected
        break
      else
        turtle.forward()
      end
      
    end
    
    cutTree(trunkSize)
    turtle.forward()
    turn180()
    plantSaplings(trunkSize)
    turn180()
     
  end
  
  backToBase()
  
end

-- @param   trunksize     length x width
function cutTree(trunkSize)
  
  print('trunkSize is')
  print(trunkSize)

  -- get under the tree
  turtle.dig()
  turtle.forward()
  
  cutVertical()
  
  print('end of 1st vertical.')
  
  -- cut the tree
  if trunkSize == '2x2' then
    turtle.turnLeft()
    turtle.dig()
    turtle.forward()
    cutVertical()
    
    for i=1,2 do
      turtle.turnRight()
      turtle.dig()
      turtle.forward()
      cutVertical()
    end
    
    turtle.turnLeft()
           
  end
  
end

function cutVertical()

  local height = 0
  
  while true do
    if turtle.detectUp() then
      turtle.digUp()
      turtle.up()
      height = height + 1
    else
      break
    end
  end
  
  for i=1, height do
    turtle.down()
  end
end

function plantSaplings(trunkSize)

  turtle.select(slot_saplings)
  
  if trunkSize == '2x2' then
    turtle.forward()
    turtle.place()
    
    turtle.turnRight()
    turtle.forward()
    turtle.turnLeft()
    turtle.place()
    
    turtle.turnRight()
    turtle.back()
    turtle.place()
    
    turtle.turnLeft()
    turtle.back()
    turtle.place()
    
  else
    turtle.place()
  end
  
  
  -- bonemeal
  turtle.select(slot_bonemeal)
  if turtle.getItemCount(slot_bonemeal) > 0 then
    turtle.place()
  end
  
  turtle.select(1)
  
end

function backToBase()
  turtle.turnLeft()
  turtle.forward()
  turtle.forward()
  turtle.turnLeft()
  
  while true do
    
    if turtle.detect() then
      -- base detected
      turtle.turnLeft()
      turtle.forward()
      turtle.forward()
      turtle.turnRight()
      turtle.forward()
      unload()
      turn180()
      break
    else
      turtle.forward()
    end
      
  end
  
end

function turn180()
  turtle.turnRight()
  turtle.turnRight()
end

function unload()
  -- unload slots 1 to 10
  for i=1,10 do
    turtle.select(i)
    turtle.drop()
  end
end

if args[1] == nil then
  args[1] = 8
end

if args[2] == nil then
  args[2] = '1x1'
end

local trunkSize = args[2]

turtle.select(1)
cutRow(args[1], args[2])