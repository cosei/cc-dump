-- Slave Workbench Cleaner
--[[
  See workbenchcleaner-master
  Used in corners to emit a redstone signal to tell the next computer to not empty the workbench
--]]

__author__ = 'aacoba'
__version__ = '0.1'

while true do
	if rs.getInput("top") then
		rs.setOutput("right", true)
	else
		rs.setOutput("right", false)
	end
end

